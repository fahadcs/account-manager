<div class="sidebar" data-color="danger" data-background-color="white"
    data-image="{{ url('assets/img/sidebar-1.jpg') }}">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->

    <div class="logo">
        <a href="/admin/dashboard" class="simple-text logo-normal">
            Account Manager
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item active">
                <a class="nav-link" href="/admin/dashboard">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item" id="vendors">
                <a class="nav-link" href="{{ route('vendors.index') }}">
                    <i class="material-icons">person</i>
                    <p>Vendors</p>
                </a>
            </li>
            <li class="nav-item" id="customers">
                <a class="nav-link" href="{{ route('customers.index') }}">
                    <i class="material-icons">person</i>
                    <p>Customers</p>
                </a>
            </li>
            <li class="nav-item" id="accounts">
                <a class="nav-link" href="{{ route('accounts.index') }}">
                    <i class="material-icons">store</i>
                    <p>Chart Of Account</p>
                </a>
            </li>
            <li class="nav-item" id="managefr">
            <a class="nav-link set-nav-links" href="{{ route('managefr.index') }}">
                <i class="material-icons set-material-icon">store</i>
                <p>Finance Recorder</p>
            </a>
        </li>
            <li class="nav-item" id="openingbalance">
                <a class="nav-link" href="{{ route('openingbalance.index') }}">
                    <i class="material-icons">money</i>
                    <p>Opening Balance</p>
                </a>
            </li>
            <li class="nav-item" id="voucher">
                <a class="nav-link" href="{{ route('vouchers.index') }}">
                    <i class="material-icons">store</i>
                    <p>Vouchers</p>
                </a>
            </li>
            <li class="nav-item" id="ledger">
                <a class="nav-link" href="{{ route('ledgers.index') }}">
                    <i class="material-icons">money</i>
                    <p>Ledgers</p>
                </a>
            </li>
            <li class="nav-item" id="compact_ledger">
                <a class="nav-link set-nav-links" href="{{ route('compact-ledger.index') }}">
                    <i class="material-icons set-material-icon">money</i>
                    <p>Compact Ledger</p>
                </a>
            </li>
        </ul>
    </div>
</div>

<!-- side menu responsive toggle //Responsive Slide Toggle Menu// -->
<div id="mySidebar" class="set-sidebar">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    <div class="logo set-logo">
        <a href="/admin/dashboard" class="simple-text logo-normal">
            Account Manager
        </a>
    </div>
    <hr>
    <ul class="nav set-nav-responsive" data-color="danger">
        <li class="nav-item active">
            <a class="nav-link set-nav-links" href="/admin/dashboard">
                <i class="material-icons set-material-icon">dashboard</i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item" id="vendors">
            <a class="nav-link set-nav-links" href="{{ route('vendors.index') }}">
                <i class="material-icons set-material-icon">person</i>
                <p>Vendors</p>
            </a>
        </li>
        <li class="nav-item" id="customers">
            <a class="nav-link set-nav-links" href="{{ route('customers.index') }}">
                <i class="material-icons set-material-icon">person</i>
                <p>Customers</p>
            </a>
        </li>
        <li class="nav-item" id="accounts">
            <a class="nav-link set-nav-links" href="{{ route('accounts.index') }}">
                <i class="material-icons set-material-icon">store</i>
                <p>Chart Of Account</p>
            </a>
        </li>
        <li class="nav-item" id="managefr">
            <a class="nav-link set-nav-links" href="{{ route('managefr.index') }}">
                <i class="material-icons set-material-icon">store</i>
                <p>Finance Recorder</p>
            </a>
        </li>
        <li class="nav-item" id="openingbalance">
            <a class="nav-link set-nav-links" href="{{ route('openingbalance.index') }}">
                <i class="material-icons set-material-icon">money</i>
                <p>Opening Balance</p>
            </a>
        </li>
        <li class="nav-item" id="voucher">
            <a class="nav-link set-nav-links" href="{{ route('vouchers.index') }}">
                <i class="material-icons set-material-icon">store</i>
                <p>Vouchers</p>
            </a>
        </li>
        <li class="nav-item" id="ledger">
            <a class="nav-link set-nav-links" href="{{ route('ledgers.index') }}">
                <i class="material-icons set-material-icon">money</i>
                <p>Ledgers</p>
            </a>
        </li>
        <li class="nav-item" id="compact_ledger">
            <a class="nav-link set-nav-links" href="{{ route('compact-ledger.index') }}">
                <i class="material-icons set-material-icon">money</i>
                <p>Compact Ledger</p>
            </a>
        </li>
    </ul>
</div>

<div id="main">
    <button class="openbtn" onclick="openNav()">☰</button>
</div>
