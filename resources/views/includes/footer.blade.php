<footer class="footer">
    <div class="container-fluid">
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>, Developed with <i class="material-icons">favorite</i> by
            <a href="#" target="_blank"> BitSoft Solutions </a> for a better Accounting Experience.
        </div>
    </div>
</footer>