@extends('layouts.register-app')
@section('content')
<div class="wrapper wrapper-full-page">
    <div class="full-page  section-image" data-color="black" data-image="assets/img/full-screen-image-2.jpg">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="col-md-4 col-sm-6 ml-auto mr-auto">
                    <form class="form" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="card card-login">
                            <div class="card-header ">
                                <h3 class="header text-center">Register</h3>
                            </div>
                            <div class="card-body ">
                                <div class="card-body">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input id="name" type="text" placeholder="Enter Name"
                                            class="form-control @error('name') is-invalid @enderror" name="name"
                                            value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email" placeholder="Enter email"
                                            class="form-control @error('email') is-invalid @enderror" name="email"
                                            value="{{ old('email') }}" required autocomplete="email">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input id="password" type="password" placeholder="Password"
                                            class="form-control @error('password') is-invalid @enderror" name="password"
                                            required autocomplete="new-password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input id="password-confirm" type="password" name="password_confirmation"
                                            placeholder="Confirm Password" class="form-control">
                                        @error('password-confirm')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        @if (Route::has('login'))
                                        <a class="btn btn-link" href="{{ route('login') }}">
                                            {{ __("Already Have an Account?") }}
                                        </a>
                                        @endif
                                    </div>
                                    <div class="card-footer ml-auto mr-auto" align="center">
                                        <button type="submit" class="btn btn-warning btn-wd">Register</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="full-page-background" style="background-image: {{url("assets/img/full-screen-image-2.jpg")}}"></div>
    </div>
</div>
@endsection
