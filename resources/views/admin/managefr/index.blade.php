@extends('layouts.admin-app')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-danger">
                        <h4 class="card-title">Finance Recorder</h4>
                        <p class="card-category">Finance Recorders Information Form</p>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('managefr.store') }}">
                            @csrf
                            <div class="row">
                                <input type="hidden" value="{{ $existing == null ? old('id') : $existing->id }}"
                                    name="id">
                                <div class="col-md-4">
                                    <div
                                        class="form-group @if($errors->has('amount')) has-danger bmd-form-group @endif">
                                        <label class="bmd-label-floating">Amount</label>
                                        <input type="text" class="form-control"
                                            value="{{ $existing == null ? old('amount') : $existing->amount }}"
                                            name="amount">
                                        @if ($errors->has('amount'))
                                        <span class="form-control-feedback">
                                            <i class="material-icons">clear</i>
                                        </span>
                                        <strong style="color: red">{{ $errors->first('amount') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div
                                        class="form-group @if($errors->has('description')) has-danger bmd-form-group @endif">
                                        <label class="bmd-label-floating">Description</label>
                                        <textarea type="text" class="form-control"
                                            name="description">{{ $existing == null ? old('description') : $existing->description }}</textarea>
                                        @if ($errors->has('description'))
                                        <span class="form-control-feedback">
                                            <i class="material-icons">clear</i>
                                        </span>
                                        <strong style="color: red">{{ $errors->first('description') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            <button type="submit" class="btn btn-danger pull-right">Save</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-danger">
                        <h4 class="card-title ">Finance Records Information</h4>
                        <p class="card-category"> Here All Finance Records Management Available</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="tblDetails">
                                <thead class="text-primary">
                                    <th>
                                        Amount
                                    </th>
                                    <th>
                                        Description
                                    </th>
                                    <th>
                                        Created At
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div id="delete_model" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Do You Really Want to Delete This Finance Recorder</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>This Record Will be deleted Permanently</p>
            </div>
            <div class="modal-footer">
                <button type="button" id="delete_btn" class="btn btn-danger">Yes Delete It!</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="delete_id">
@endsection
@section('script')
<script>
    $(document).ready(function () {
            $('#tblDetails').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('managefr.data') }}",
                columns: [
                    {data: 'amount', name: 'amount'},
                    {data: 'description', name: 'description'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            $('.nav li.active').removeClass('active');
            $('#managefr').addClass('active');
        });
        function Confirm(id) {
            $('#delete_id').val(id);
            $('#delete_model').modal('show');
        }

        $('#delete_btn').click(function (e) {
           var id = $('#delete_id').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type: 'POST',
               data: {
                   _method: 'DELETE'
               },
                url:'managefr/'+id,
                success: function (data) {
                    location.reload();
                }
            });
        });
</script>
@endsection
