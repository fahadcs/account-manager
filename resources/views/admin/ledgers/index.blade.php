@extends('layouts.admin-app')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-danger">
                        <h4 class="card-title">Ledgers</h4>
                        <p class="card-category">Ledger Report Form</p>
                    </div>
                    <div class="card-body">
                        <form action="">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="accountname">Account Name</label>
                                    <select id="accountname" class="form-control js-example-basic-single"
                                        name="accountname ">
                                        <option value="0">Select One</option>
                                        @foreach ($accountname as $item)
                                        <option value="{{$item->id}}">{{$item->account_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label style=" margin-top: 10px" for="date">StartDate</label>
                                    <input autocomplete="off"
                                        style="margin-top: 30px;position: relative; z-index: 100000;"
                                        placeholder="Select Date" id="startDate" name="datepicker[]" type="text"
                                        class="form-control datepicker" required="" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label style=" margin-top: 10px" for="date">EndDate</label>
                                    <input autocomplete="off"
                                        style="margin-top: 30px; position: relative; z-index: 100000;"
                                        placeholder="Select Date" id="endDate" name="datepicker[]" type="text"
                                        class="form-control datepicker" required="" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-danger">
                    <h4 class="card-title ">Ledger Report Information</h4>
                    <p class="card-category"> Here All Ledger Report Information Available</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLedgerDetails">
                            <thead class="text-primary">
                                <th>
                                    ID
                                </th>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Voucher Type
                                </th>
                                <th>
                                    Account Name
                                </th>
                                <th>
                                    Narration
                                </th>
                                <th>
                                    Debit
                                </th>
                                <th>
                                    Credit
                                </th>
                                <th>
                                    Balance
                                </th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    function populateDatatable() {
        //Access Dropdown
        //Access Start Date
        //Access End Date
        //Server Conditions:
        //if all is none: Show All Data
        //If account is selected and dates are not, Then show data for that accuont
        //If Dates are selected without account then show all transactions within date
        //If all are selected then show filtered Data:
        var account =$('#accountname :selected').val();
        var startDate =$('#startDate').val();
        var endDate =$('#endDate').val();


        var table =$('#tblLedgerDetails').DataTable({
            processing: true,
            serverSide: true,
            ajax:{ type: 'POST', url: "{{ route('ledger.data') }}",
            data: { "_token": "{{ csrf_token() }}", "account":account, "startDate":startDate, "endDate":endDate }},
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'date', name: 'date'},
                    {data: 'voucher_type', name: 'voucher_type'},
                    {data: 'account_name', name: 'account_name'},
                    {data: 'narration', name: 'narration'},
                    {data: 'debit', name: 'debit'},
                    {data: 'credit', name: 'credit'},
                    {data: 'balance', name: 'balance'},
                ]
            });
    }
    function checkDate(){
        $("#startDate").datepicker();
        $("#endDate").datepicker();
        var startDate =$('#startDate').val();
        var endDate =$('#endDate').val();
            if(startDate!=="" && endDate!=="")
                {
                    if(startDate<endDate)
                    {
                        $('#tblLedgerDetails').DataTable().clear().destroy();
                        populateDatatable();
                    }
                }
            else
            {
                $('#tblLedgerDetails').DataTable().clear().destroy();
                populateDatatable();
            }
    }

    $(document).ready(function () {

            $('.nav li.active').removeClass('active');
            $('#ledger').addClass('active');
            populateDatatable();
            $('.js-example-basic-single').select2();/////Select with search box
            $('select').on('change', function() {

                checkDate();
            });




            $("#startDate").datepicker();
            $("#endDate").datepicker();
            $("#startDate").on("change",function(){
                checkDate();
            });
            $("#endDate").on("change",function(){

                checkDate();
            });
});
</script>
@endsection
