@extends('layouts.admin-app')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-danger">
                        <h4 class="card-title">Ledgers</h4>
                        <p class="card-category">Ledger Report Form</p>
                    </div>
                    <div class="card-body">
                        <form action="">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="accountname">Account Name</label>
                                    <select id="accountname" class="form-control js-example-basic-single"
                                        name="accountname ">
                                        <option value="0">Select One</option>
                                        @foreach ($accountname as $item)
                                        <option value="{{$item->id}}">{{$item->account_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label style=" margin-top: 10px" for="date">StartDate</label>
                                    <input autocomplete="off"
                                        style="margin-top: 30px;position: relative; z-index: 100000;"
                                        placeholder="Select Date" id="startDate" name="datepicker[]" type="text"
                                        class="form-control datepicker" required="" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label style=" margin-top: 10px" for="date">EndDate</label>
                                    <input autocomplete="off"
                                        style="margin-top: 30px; position: relative; z-index: 100000;"
                                        placeholder="Select Date" id="endDate" name="datepicker[]" type="text"
                                        class="form-control datepicker" required="" />
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-danger">
                    <h4 class="card-title ">Ledger Report Information</h4>
                    <p class="card-category"> Here All Ledger Report Information Available</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblCompactLedgerDetails">
                            <thead class="text-primary">

                                <th>
                                    Account Name
                                </th>

                                <th>
                                    Debit
                                </th>
                                <th>
                                    Credit
                                </th>

                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    function populateDatatable() {

        var account =$('#accountname :selected').val();
        var length = $('#accountname > option').length;
        var startDate =$('#startDate').val();
        var endDate =$('#endDate').val();
        //For checking if there is any other option except "Select One"
        if(length>1)
        {
            var table =$('#tblCompactLedgerDetails').DataTable({
            processing: true,
            serverSide: true,
            ajax:{ type: 'POST', url: "{{ route('compactledger.data') }}",
            data: { "_token": "{{ csrf_token() }}", "account":account, "startDate":startDate, "endDate":endDate }},
                columns: [
                    {data: 'account_name', name: 'account_name'},
                    {data: 'debit_sum', name: 'debit_sum'},
                    {data: 'credit_sum', name: 'credit_sum'},
                ]
            });
        }

    }

    function checkDate(){
        $("#startDate").datepicker();
        $("#endDate").datepicker();
        var startDate =$('#startDate').val();
        var endDate =$('#endDate').val();

            if(startDate!=="" && endDate!=="")
                {
                    if(startDate<endDate)
                    {
                        $('#tblCompactLedgerDetails').DataTable().clear().destroy();
                        populateDatatable();
                    }
                }
                else if(startDate==="" && endDate!==""){
                    return;
                }
                else if(startDate!=="" && endDate===""){
                    return;
                }
            else
            {
                $('#tblCompactLedgerDetails').DataTable().clear().destroy();
                populateDatatable();
            }
    }
    $(document).ready(function () {

            $('.nav li.active').removeClass('active');
            $('#compact_ledger').addClass('active');
            populateDatatable();
            $('.js-example-basic-single').select2();/////Select with search box
            $('select').on('change', function() {

                checkDate();
            });
            $("#startDate").datepicker();
            $("#endDate").datepicker();
            $("#startDate").on("change",function(){
                checkDate();
            });
            $("#endDate").on("change",function(){

                checkDate();
            });
});
</script>
@endsection
