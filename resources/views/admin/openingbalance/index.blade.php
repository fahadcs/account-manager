@extends('layouts.admin-app')
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-danger">
                            <h4 class="card-title">Opening Balance</h4>
                            <p class="card-category">Opening Balance Form</p>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ route('openingbalance.store') }}">
                                @csrf
                                <div class="row">
                                    <input type="hidden" value="{{ $existing == null ? old('id') : $existing->id }}" name="id">
                                    <div class="col-md-6">
                                        <div class="form-group @if($errors->has('account_type')) has-danger bmd-form-group @endif">

                                            <select class="form-control" name="account_id">

                                                  <option @if($existing == null) selected @endif value="0">-- Select Account --</option>
                                                @if($existing == null)
                                                    @foreach($accounts_name as $name)
                                                         @if($name->is_parent == false && $name->account_id == null)
                                                            <option value="{{ $name->id }}" @if($existing != null && $existing->account_id == $name->id) selected @endif>
                                                            {{ $name->account_name}}
                                                           </option>
                                                        @endif
                                                      @endforeach
                                                 @else
                                                    @foreach($accounts_name as $name)
                                                        @if($name->is_parent == false)
                                                            <option value="{{ $name->id }}" @if($existing != null && $existing->account_id == $name->id) selected @endif>
                                                                {{ $name->account_name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                 @endif
                                            </select>
                                            @if ($errors->has('account_name'))
                                                <span class="form-control-feedback">
                                                    <i class="material-icons">clear</i>
                                                </span>
                                                <strong style="color: red">{{ $errors->first('account_name') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group @if($errors->has('amount')) has-danger bmd-form-group @endif">
                                            <label class="bmd-label-floating">Balance Amount</label>
                                            <input type="number" class="form-control"
                                                   value="{{ $existing == null ? old('debit_limit') : $amount }}"
                                                   name="amount">
                                            @if ($errors->has('amount'))
                                                <span class="form-control-feedback">
                                                    <i class="material-icons">clear</i>
                                                </span>
                                                <strong style="color: red">{{ $errors->first('amount') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <button type="Submit" value="debit" name="submit" class="btn btn-danger pull-left">Debit Amount</button>
                                <button type="Submit" value="credit" name="submit" class="btn btn-danger pull-left">Credit Amount</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-danger">
                            <h4 class="card-title ">Opening Balance</h4>
                            <p class="card-category"> Here All Opening Balance Information Available</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="tblDetails">
                                    <thead class="text-primary">
                                    <th>
                                        Account Name
                                    </th>
                                    <th>
                                        Debit
                                    </th>
                                    <th>
                                        Credit
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delete_model" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Do You Really Want to Delete This Opening Balance Record</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This Opening Balance Record Will be deleted Permanently</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="delete_btn" class="btn btn-danger">Yes Delete It!</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="delete_id">
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#tblDetails').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('openingbalance.data') }}",
                columns: [
                    {data: 'account_name', name: 'account_name'},
                    {data: 'debit', name: 'debit'},
                    {data: 'credit', name: 'credit'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            $('.nav li.active').removeClass('active');
            $('#openingbalance').addClass('active');
        });
        function Confirm(id) {
            $('#delete_id').val(id);
            $('#delete_model').modal('show');
        }

        $('#delete_btn').click(function (e) {
           var id = $('#delete_id').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type: 'POST',
               data: {
                   _method: 'DELETE'
               },
                url:'openingbalance/'+id,
                success: function (data) {
                    location.reload();
                }
            });
        });
    </script>
@endsection