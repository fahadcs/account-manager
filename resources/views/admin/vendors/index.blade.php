@extends('layouts.admin-app')
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-danger">
                            <h4 class="card-title">Vendors</h4>
                            <p class="card-category">Vendor Information Form</p>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ route('vendors.store') }}">
                                @csrf
                                <div class="row">
                                    <input type="hidden" value="{{ $existing == null ? old('id') : $existing->id }}" name="id">
                                    <div class="col-md-3">
                                        <div class="form-group @if($errors->has('name')) has-danger bmd-form-group @endif">
                                            <label class="bmd-label-floating">Vendor Name</label>
                                            <input type="text" class="form-control"
                                                   value="{{ $existing == null ? old('name') : $existing->name }}"
                                                   name="name">
                                            @if ($errors->has('name'))
                                                <span class="form-control-feedback">
                                                    <i class="material-icons">clear</i>
                                                </span>
                                                <strong style="color: red">{{ $errors->first('name') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group @if($errors->has('company_name')) has-danger bmd-form-group @endif">
                                            <label class="bmd-label-floating">Company Name</label>
                                            <input type="text" class="form-control"
                                                   value="{{ $existing == null ? old('company_name') : $existing->company_name }}"
                                                   name="company_name">
                                            @if ($errors->has('company_name'))
                                                <span class="form-control-feedback">
                                                    <i class="material-icons">clear</i>
                                                </span>
                                                <strong style="color: red">{{ $errors->first('company_name') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group @if($errors->has('email')) has-danger bmd-form-group @endif">
                                            <label class="bmd-label-floating">Email Address</label>
                                            <input type="text" class="form-control"
                                                   value="{{ $existing == null ? old('email') : $existing->email }}"
                                                   name="email">
                                            @if ($errors->has('email'))
                                                <span class="form-control-feedback">
                                                    <i class="material-icons">clear</i>
                                                </span>
                                                <strong style="color: red">{{ $errors->first('email') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group @if($errors->has('contact_number')) has-danger bmd-form-group @endif">
                                            <label class="bmd-label-floating">Phone Number</label>
                                            <input type="text" class="form-control"
                                                   value="{{ $existing == null ? old('contact_number') : $existing->contact_number }}"
                                                   name="contact_number">
                                            @if ($errors->has('contact_number'))
                                                <span class="form-control-feedback">
                                                    <i class="material-icons">clear</i>
                                                </span>
                                                <strong style="color: red;">{{ $errors->first('contact_number') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group @if($errors->has('debit_limit')) has-danger bmd-form-group @endif">
                                            <label class="bmd-label-floating">Debit Limit (RS)</label>
                                            <input type="text" class="form-control"
                                                   value="{{ $existing == null ? old('debit_limit') : $existing->debit_limit }}"
                                                   name="debit_limit">
                                            @if ($errors->has('debit_limit'))
                                                <span class="form-control-feedback">
                                                    <i class="material-icons">clear</i>
                                                </span>
                                                <strong style="color: red">{{ $errors->first('debit_limit') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-danger pull-right">Save</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-danger">
                            <h4 class="card-title ">Simple Table</h4>
                            <p class="card-category"> Here is a subtitle for this table</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="tblDetails">
                                    <thead class="text-primary">
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Company Name
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Phone Number
                                    </th>
                                    <th>
                                        Debit Limit
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delete_model" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Do You Really Want to Delete This Item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This Item Will be deleted Permanently</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="delete_btn" class="btn btn-danger">Yes Delete It!</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="delete_id">
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#tblDetails').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('vendor.data') }}",
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'company_name', name: 'company_name'},
                    {data: 'email', name: 'email'},
                    {data: 'contact_number', name: 'contact_number'},
                    {data: 'debit_limit', name: 'debit_limit'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            $('.nav li.active').removeClass('active');
            $('#vendors').addClass('active');
        });
        function Confirm(id) {
            $('#delete_id').val(id);
            $('#delete_model').modal('show');
        }

        $('#delete_btn').click(function (e) {
           var id = $('#delete_id').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type: 'POST',
               data: {
                   _method: 'DELETE'
               },
                url:'vendors/'+id,
                success: function (data) {
                    location.reload();
                }
            });
        });
    </script>
@endsection