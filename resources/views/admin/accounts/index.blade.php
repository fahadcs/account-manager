@extends('layouts.admin-app')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-danger">
                        <h4 class="card-title">Chart Of Accounts</h4>
                        <p class="card-category">Chart Of Accounts Information Form</p>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('accounts.store') }}">
                            @csrf
                            <div class="row">
                                <input type="hidden" value="{{ $existing == null ? old('id') : $existing->id }}"
                                    name="id">
                                <div class="col-md-4">
                                    <div
                                        class="form-group @if($errors->has('account_name')) has-danger bmd-form-group @endif">
                                        <label class="bmd-label-floating">Account Name</label>
                                        <input type="text" class="form-control"
                                            value="{{ $existing == null ? old('account_name') : $existing->account_name }}"
                                            name="account_name">
                                        @if ($errors->has('account_name'))
                                        <span class="form-control-feedback">
                                            <i class="material-icons">clear</i>
                                        </span>
                                        <strong style="color: red">{{ $errors->first('account_name') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div
                                        class="form-group @if($errors->has('account_type')) has-danger bmd-form-group @endif">
                                        <select class="form-control" name="account_type">
                                            <option @if($existing==null) selected @endif value="0"> -- Select Account
                                                Type -- </option>
                                            <option @if(old('account_type')=='Assets' ) selected @endif @if($existing
                                                !=null && $existing->account_type == 'Assets') selected @endif
                                                value="Assets">Assets</option>
                                            <option @if(old('account_type')=='Liability' ) selected @endif @if($existing
                                                !=null && $existing->account_type == 'Liability') selected @endif
                                                value="Liability">Liability</option>
                                            <option @if(old('account_type')=='Expense' ) selected @endif @if($existing
                                                !=null && $existing->account_type == 'Expense') selected @endif
                                                value="Expense">Expense</option>
                                            <option @if(old('account_type')=='Capital' ) selected @endif @if($existing
                                                !=null && $existing->account_type == 'Capital') selected @endif
                                                value="Capital">Capital</option>
                                            <option @if(old('account_type')=='Revenue' ) selected @endif @if($existing
                                                !=null && $existing->account_type == 'Revenue') selected @endif
                                                value="Revenue">Revenue</option>
                                        </select>
                                        @if ($errors->has('account_type'))
                                        <span class="form-control-feedback">
                                            <i class="material-icons">clear</i>
                                        </span>
                                        <strong style="color: red">{{ $errors->first('account_type') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div
                                        class="form-group @if($errors->has('parent_id')) has-danger bmd-form-group @endif">
                                        <select class="form-control" name="parent_id">
                                            <option @if(count($accounts)==0) selected @endif value="0"> -- Select Parent
                                                Account -- </option>
                                            @foreach($accounts as $account)
                                            <option value="{{ $account->id }}" @if($existing !=null && $existing->
                                                parent_id == $account->id) selected @endif>
                                                {{ $account->account_name }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('parent_id'))
                                        <span class="form-control-feedback">
                                            <i class="material-icons">clear</i>
                                        </span>
                                        <strong style="color: red">{{ $errors->first('parent_id') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div
                                        class="form-check @if($errors->has('is_parent')) has-danger bmd-form-group @endif">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="is_parent"
                                                @if($existing !=null && $existing->is_parent == true) checked @endif >
                                            Is Parent ?
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                        @if ($errors->has('is_parent'))
                                        <span class="form-control-feedback">
                                            <i class="material-icons">clear</i>
                                        </span>
                                        <strong style="color: red">{{ $errors->first('is_parent') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-danger pull-right">Save</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-danger">
                        <h4 class="card-title ">Chart Of Account Information</h4>
                        <p class="card-category"> Here All Chart Of Account Information Available</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="tblDetails">
                                <thead class="text-primary">
                                    <th>
                                        Account Name
                                    </th>
                                    <th>
                                        Account Type
                                    </th>
                                    <th>
                                        Is Parent ?
                                    </th>
                                    <th>
                                        Parent Account
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="delete_model" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Do You Really Want to Delete This Chart Of Account Record</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>This Record Will be deleted Permanently</p>
            </div>
            <div class="modal-footer">
                <button type="button" id="delete_btn" class="btn btn-danger">Yes Delete It!</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="delete_id">
@endsection
@section('script')
<script>
    $(document).ready(function () {
            $('#tblDetails').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('account.data') }}",
                columns: [
                    {data: 'account_name', name: 'account_name'},
                    {data: 'account_type', name: 'account_type'},
                    {data: 'is_parent', name: 'is_parent'},
                    {data: 'parent_name', name: 'parent_name'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            $('.nav li.active').removeClass('active');
            $('#accounts').addClass('active');
        });
        function Confirm(id) {
            $('#delete_id').val(id);
            $('#delete_model').modal('show');
        }

        $('#delete_btn').click(function (e) {
           var id = $('#delete_id').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type: 'POST',
               data: {
                   _method: 'DELETE'
               },
                url:'accounts/'+id,
                success: function (data) {
                    location.reload();
                }
            });
        });
</script>
@endsection
