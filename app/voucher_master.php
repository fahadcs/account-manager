<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class voucher_master extends Model
{
    protected $table='voucher_masters';
    protected $fillable=['date','voucher_type'];
}
