<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class VerifyToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if($request->header('Authorization')!= ""){
            $token = $request->header('Authorization');
            $token = ltrim($token, "Token ");
            $user = User::where('api_token', $token)
                            ->first();

            if(!$user)
            return response()->json([
                'success'=>false, 
                'message' => "Invalid Token",
            ]);
        }
        else {
            return response()->json([
                'success'=>false, 
                'message' => "Error: Provide Token",
            ]);
        }
        if($request['user_id'] == ''){
            $request['user_id'] = $user->id;
        }
        return $next($request);
    }
}
