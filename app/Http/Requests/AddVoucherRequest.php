<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddVoucherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'date'=>'required',
             'voucher_type'=>'required',
             'account_name'=>'required',
             'account_id'=>'required',
             'narration'=>'required',
             'debit'=>'required',
             'credit'=>'required'
        ];
    }
}
