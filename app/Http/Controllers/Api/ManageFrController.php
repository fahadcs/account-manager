<?php

namespace App\Http\Controllers\API;

use App\FinanceRecorder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\ManageFrResource;

class ManageFrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $financeRecorders = FinanceRecorder::all();
        return response([ 'data' => ManageFrResource::collection($financeRecorders), 'message' => 'Retrieved successfully'], 200);
  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'user_id' => 'required',
            'amount' => 'required',
            'description' => 'required'
        ]);
        
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
        }
        
        $financeRecord = FinanceRecorder::create($data);

        return response(['financeRecord' => new ManageFrResource($financeRecord), 'message' => 'Created successfully'], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FinanceRecorder  $financeRecorder
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $financeRecorder = FinanceRecorder::find($id);
        if (!$financeRecorder) {
            return response()->json([
                'success' => false,
                'message' => 'Record with id ' . $id . ' not found'
            ], 400);
        }
        return response(['data' => new ManageFrResource($financeRecorder), 'message' => 'Retrieved successfully'], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FinanceRecorder  $financeRecorder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $financeRecorder = FinanceRecorder::find($id);
        if (!$financeRecorder) {
            return response()->json([
                'success' => false,
                'message' => 'Record with id ' . $id . ' not found'
            ], 400);
        }

        $financeRecorder->update($request->all());
        return response(['data' => new ManageFrResource($financeRecorder), 'message' => 'Updated successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FinanceRecorder  $financeRecorder
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $row = FinanceRecorder::find($id);
        if (!$row) {
            return response()->json([
                'success' => false,
                'message' => 'Record with id ' . $id . ' not found'
            ], 400);
        }
        $row->delete();
        return response(['success' => true, 'message' => 'Deleted']);
    }
}
