<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Account;
use App\voucher_master;
use App\VoucherDetail;
use Validator;
use Session;
use Redirect;
use DB;
use DataTables;
use SebastianBergmann\Environment\Console;

class LedgerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $existing = null;
        $user = auth()->user();
        $accountname = Account::select('id', 'account_name', 'user_id')->where('user_id', $user->id)->where('is_parent', '=', false)->get();
        return view('admin.ledgers.index', compact('accountname', 'existing'));

    }
    public function data(Request $request)
    {
        //echo '<pre>'; print_r($request->get('startDate')); exit;

        if(empty($request->account)&&empty($request->startDate)&&empty($request->endDate))
        {
            $user = auth()->user();
            $record = voucher_master::join('voucher_details', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_masters.id', 'voucher_masters.date', 'voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','accounts.account_name')
            ->where('accounts.user_id','=', $user->id)->orderBy('voucher_details.id', 'asc')->distinct()->get();
            $balance=0;
            foreach ($record as $key => $r) {
                $balance=($r->debit - $r->credit) + $balance;
                $r->setAttribute('balance', $balance);
            }
            return DataTables::of($record)
            ->make(true);
        }
        elseif (empty($request->startDate)&&empty($request->endDate)) {
            $user = auth()->user();
            $record = voucher_master::join('voucher_details', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_masters.id', 'voucher_masters.date', 'voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','accounts.account_name')
            ->where([
                ['accounts.user_id','=', $user->id],
                ['voucher_details.account_id','=', $request->account]
                ])
                ->orderBy('voucher_details.id', 'asc')
                ->distinct()->get();
            $balance=0;
            foreach ($record as $key => $r) {
                $balance=($r->debit - $r->credit) + $balance;
                $r->setAttribute('balance', $balance);
            }
            return DataTables::of($record)
            ->make(true);
        }
        elseif (empty($request->account)) {
            $user = auth()->user();
            $record = voucher_master::join('voucher_details', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_masters.id', 'voucher_masters.date', 'voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','accounts.account_name')
            ->where('accounts.user_id','=', $user->id)
            ->whereBetween('voucher_masters.date', [
                date("Y-m-d", strtotime($request->startDate)),
                date("Y-m-d", strtotime($request->endDate))])
            ->orderBy('voucher_details.id', 'asc')
            ->distinct()->get();
            $balance=0;
            foreach ($record as $key => $r) {
                $balance=($r->debit - $r->credit) + $balance;
                $r->setAttribute('balance', $balance);
            }
            return DataTables::of($record)
            ->make(true);
        }
        else {
            $user = auth()->user();
            $record = voucher_master::join('voucher_details', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_masters.id', 'voucher_masters.date', 'voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','accounts.account_name')
            ->where([
                ['accounts.user_id','=', $user->id],
                ['voucher_details.account_id','=', $request->account]
                ])
            ->whereBetween('voucher_masters.date', [
                date("Y-m-d", strtotime($request->startDate)),
                date("Y-m-d", strtotime($request->endDate))])
            ->orderBy('voucher_details.id', 'asc')
            ->distinct()->get();
            $balance=0;
            foreach ($record as $key => $r) {
                $balance=($r->debit - $r->credit) + $balance;
                $r->setAttribute('balance', $balance);
            }
            return DataTables::of($record)
            ->make(true);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
