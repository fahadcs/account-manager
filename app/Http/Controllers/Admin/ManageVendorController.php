<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vendor;
use DataTables;
class ManageVendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $existing = null;
        return view('admin.vendors.index',compact('existing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:120|min:3',
            'company_name' => 'required|max:120|min:3',
            'email' => 'required|email',
            'debit_limit' => 'required|numeric',
            'contact_number' => 'required|regex:/[0-9]{11}/',
        ]);
        if($request->input('id'))
        {
            $id = $request->input('id');
            $existing = Vendor::find($id);
            if($existing)
            {
                $existing->name = $request->input('name');
                $existing->company_name = $request->input('company_name');
                $existing->email = $request->input('email');
                $existing->contact_number = $request->input('contact_number');
                $existing->debit_limit = $request->input('debit_limit');
                $existing->save();
            }
            return redirect(route('vendors.index'));
        }
        $this->validate($request,[
            'name' => 'required|max:120|min:3',
            'company_name' => 'required|max:120|min:3',
            'email' => 'required|email|unique:vendors',
            'debit_limit' => 'required|numeric',
            'contact_number' => 'required|regex:/[0-9]{11}/',
        ]);
        $vendor = new Vendor();
        $vendor->name = $request->input('name');
        $vendor->company_name = $request->input('company_name');
        $vendor->email = $request->input('email');
        $vendor->contact_number = $request->input('contact_number');
        $vendor->debit_limit = $request->input('debit_limit');
        $vendor->save();
        return redirect(route('vendors.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $existing = Vendor::find($id);
        if($existing)
        {
            return view('admin.vendors.index',compact('existing'));
        }
        return "Error";
    }

    public function data()
    {
        $record = Vendor::all();
        return DataTables::of($record)
            ->addColumn('action', function ($row){
                return '<a class="btn btn-primary btn-sm" href="'. route('vendors.edit',['customer' => $row->id ]) . '">Edit<div class="ripple-container"></div></a>
                        <button onclick="return Confirm('.$row->id. ')" class="btn btn-danger btn-sm">Delete<div class="ripple-container"></div></button>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existing = Vendor::find($id);
        if($existing)
        {
            $existing->delete();
            return response()->json(['data'=>true]);
        }
        else
        {
            return response()->json(['data'=>false]);
        }
    }
}
