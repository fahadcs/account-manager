<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//namespace App\Http\Controllers\Auth;
use App\Account;
use DataTables;
use Validator;
use Illuminate\Support\Facades\Auth;

class ManageAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $existing = null;
        $accounts = Account::where('is_parent','=',true)->where('user_id','=',Auth::user()->id)->get();
        return view('admin.accounts.index',compact('existing','accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = Validator::make($request->all(), [
            'account_name' => 'required|max:120|min:3',
        ]);
       if($request->input('account_type') == '0')
       {
           $validator->after(function ($validator){
               $validator->errors()->add('account_type','Account Type Field is required');
           });
       }
       $is_parent = $request->has('is_parent') ? true : false;
       if($is_parent == false)
       {
           if($request->input('parent_id') == '0')
           {
               $validator->after(function ($validator){
                  $validator->errors()->add('parent_id','Parent Account is Required');
               });
           }
       }
        if($validator->fails())
        {
            return redirect(route('accounts.index'))->withErrors($validator)->withInput();
        }
        if($request->input('id'))
        {
            $id = $request->input('id');
            $existing = Account::find($id);
            if($existing)
            {
                $existing->account_name = $request->input('account_name');
                $existing->account_type = $request->input('account_type');
                $existing->is_parent = $is_parent;
                $existing->parent_id = $request->input('parent_id');
                $existing->user_id = Auth::user()->id;//this->id;
                $existing->save();
            }
            return redirect(route('accounts.index'));
        }
        $account = new Account();
        $account->account_name = $request->input('account_name');
        $account->account_type = $request->input('account_type');
        $account->is_parent = $is_parent;
        $account->parent_id = $request->input('parent_id');
        $account->user_id = Auth::user()->id;//this->id;
        $account->save();
        return redirect(route('accounts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $existing = Account::find($id);
        $accounts = Account::where('is_parent','=',true)->where('user_id','=',Auth::user()->id)->get();
        if($existing)
        {
            return view('admin.accounts.index',compact('existing','accounts'));
        }
        return "Error";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function data()
    {
       $user_id=Auth::user()->id;
       $record = Account::where('user_id',$user_id)->get();
        return DataTables::of($record)
            ->addColumn('action', function ($row){
                return '<a class="btn btn-primary btn-sm" href="'. route('accounts.edit',['account' => $row->id ]) . '">Edit<div class="ripple-container"></div></a>
                        <button onclick="return Confirm('.$row->id. ')" class="btn btn-danger btn-sm">Delete<div class="ripple-container"></div></button>';
            })
            ->editColumn('is_parent',function ($row){
                $is_parent = "";
                if($row->is_parent){
                    $is_parent = "Yes";
                }
                else{
                    $is_parent = "No";
                }
                return $is_parent;
            })
            ->addColumn('parent_name',function ($row){
                $parent_name = "";
                if($row->parent_id != 0)
                {
                    $acc = Account::find($row->parent_id);
                    if($acc){
                        $parent_name = $acc->account_name;
                    }
                }
                else
                    {
                        $parent_name = "NA";
                    }
                    return $parent_name;
            })
            ->rawColumns(['action','is_parent','parent_name'])
            ->make(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $existing = Account::find($id);


        if($existing)
        {
            $existing->delete();
            Account::where('parent_id', $id)->delete();
            return response()->json(['data'=>true]);
        }
        else
        {
            return response()->json(['data'=>false]);
        }
    }
}
