<?php

namespace App\Http\Controllers\Admin;
//namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use App\Http\Controllers\Admin\Validator;
use App\Account;
use App\voucher_master;
use App\VoucherDetail;
use Validator;
use Session;
use Redirect;
use DB;
use DataTables;

session_start();
class ManageVoucherController extends Controller
{
    public function data()
    {
        $user = auth()->user();

        $record = DB::table('voucher_masters')
            ->select('voucher_masters.id', 'voucher_masters.date', 'voucher_masters.voucher_type')
            ->join('voucher_details', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->where(['accounts.user_id' => $user->id])->distinct()->get();


        return DataTables::of($record)
            ->addColumn('debit_sum', function ($row) {
                $voucher_details = VoucherDetail::where('voucher_master_id', $row->id)->get();
                $debit_sum = 0;
                foreach ($voucher_details as $key => $value) {
                    $debit_sum += $value->debit;
                }
                return $debit_sum;
            })
            ->addColumn('credit_sum', function ($row) {
                $voucher_details = VoucherDetail::where('voucher_master_id', $row->id)->get();
                $credit_sum = 0;
                foreach ($voucher_details as $key => $value) {
                    $credit_sum += $value->credit;
                }
                return $credit_sum;
            })
            ->addColumn('action', function ($row) {
                return '<a class="btn btn-primary btn-sm" href="' . route('vouchers.edit', ['voucher' => $row->id]) . '">Edit<div class="ripple-container"></div></a>
                        <button onclick="return Confirm(' . $row->id . ')" class="btn btn-danger btn-sm">Delete<div class="ripple-container"></div></button>
                        <a class="btn btn-warning btn-sm" onclick="return showDetails(' . $row->id . ')" >Detail<div class="ripple-container"></div></a>';
            })
            ->rawColumns(['action', 'debit_sum', 'credit_sum'])
            ->make(true);
    }
    public function edit($id)
    {
        $user = auth()->user();
        $existing = voucher_master::find($id);
        $masterID = $id;
        $detailexist = DB::table('voucher_details')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_details.id as vid', 'voucher_details.narration as narration', 'voucher_details.debit as debit', 'voucher_details.credit as credit', 'accounts.account_name as account_name', 'voucher_details.account_id as accountid')
            ->where(['voucher_details.voucher_master_id' => $id])->get();
        //$detailexist = VoucherDetail::where('voucher_master_id',$id)->get();

        $accountname = Account::select('id', 'account_name', 'user_id')->where('user_id', $user->id)->where('is_parent', '=', false)->get();

        if ($existing) {
            Session::put("voucher_master_id", $id);
            return view('admin.vouchers.index', compact('existing', 'detailexist', 'accountname', 'masterID'));
        }
        return "Error";
    }
    public function destroy($id)
    {

        $existing = voucher_master::find($id);

        if ($existing) {
            $existing->delete();
            VoucherDetail::where('voucher_master_id', $id)->delete();
            return response()->json(['data' => true]);
        } else {
            return response()->json(['data' => false]);
        }
    }
    public function showdetail(Request $request)
    {
        $record = DB::table('voucher_details')
            ->join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
            ->select('voucher_details.narration as narration', 'voucher_details.debit as debit', 'voucher_details.credit as credit', 'accounts.account_name as account_name')
            ->where(['voucher_details.voucher_master_id' => $request->voucher_master_id])->get();


        $voucher_details = VoucherDetail::where('voucher_master_id', $request->voucher_master_id)->get();
        $debit_sum = 0;
        foreach ($voucher_details as $key => $value) {
            $debit_sum += $value->debit;
        }

        $voucher_details = VoucherDetail::where('voucher_master_id', $request->voucher_master_id)->get();
        $credit_sum = 0;
        foreach ($voucher_details as $key => $value) {
            $credit_sum += $value->credit;
        }
        $vmaster = voucher_master::find($request->voucher_master_id);
        $vmaster->setAttribute('debit_sum', $debit_sum);
        $vmaster->setAttribute('credit_sum', $credit_sum);


        return response()->json(['success' => true, 'record' => $record, 'voucher_master' => $vmaster]);




        //echo '<pre>'; print_r($record); exit;

    }
    public function saveform()
    {

        if (Session::get('voucher_master_id')) {
            session()->forget('voucher_master_id');
            //unset($_SESSION['voucher_master_id']);
            //return back();
            //window.location.reload();
            return response()->json(['success' => true]);
            //return response()->json(['success'=>true]);
        }
        return response()->json(['success' => false]);
    }
    public function delete(Request $request)
    {

        $voucher_detail = VoucherDetail::find($request->voucher_detail_id);
        $voucher_detail->delete();
    }
    public function index()
    {
        $existing = null;
        $user = auth()->user();
        $accountname = Account::select('id', 'account_name', 'user_id')->where('user_id', $user->id)->where('is_parent', '=', false)->get();
        return view('admin.vouchers.index', compact('accountname', 'existing'));
    }
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'voucher_type' => 'required',
            'account_name' => 'required',
            'account_id' => 'required',
            'narration' => 'required',
            'debit' => 'required',
            'credit' => 'required'
        ]);

        if ($validator->passes()) {
            if (!Session::get('voucher_master_id')) {
                $voucher_master = new voucher_master;
                $voucher_master->date = date("Y-m-d", strtotime($request->date));
                $voucher_master->voucher_type = $request->input('voucher_type');
                $voucher_master->save();

                Session::put("voucher_master_id", $voucher_master->id);
            } else {
                $voucher_master = voucher_master::find(Session::get('voucher_master_id'));
                $voucher_master->date = date("Y-m-d", strtotime($request->date));
                $voucher_master->voucher_type = $request->input('voucher_type');
                $voucher_master->save();
            }
            //echo "<pre>"; print_r($request->all()); exit;
            //  echo '<pre>'; print_r($request->all()); exit;

            //$voucher_master->id;
            //echo($voucher_master->id);

            if ($request->input('voucher_detail_id')) {
                $voucher_detail = VoucherDetail::find($request->voucher_detail_id);
                $voucher_detail->account_id = $request->input('account_id');
                $voucher_detail->narration = $request->input('narration');
                $voucher_detail->debit = $request->input('debit');
                $voucher_detail->credit = $request->input('credit');
                $voucher_detail->voucher_master_id = Session::get("voucher_master_id");
                $voucher_detail->save();
            } else {
                $voucher_detail = new VoucherDetail;
                $voucher_detail->account_id = $request->input('account_id');
                $voucher_detail->narration = $request->input('narration');
                $voucher_detail->debit = $request->input('debit');
                $voucher_detail->credit = $request->input('credit');
                $voucher_detail->voucher_master_id = Session::get("voucher_master_id");
                $voucher_detail->save();
            }

            return response()->json(['success' => true, 'voucher_detail' => $voucher_detail]);
        }

        return response()->json(['success' => false, 'errors' => $validator->errors()->all()]);

        // $this->validate($request->all(),[
        //     'date'=>'required',
        //     'voucher_type'=>'required',
        //     'account_name'=>'required',
        //     'account_id'=>'required',
        //     'narration'=>'required',
        //     'debit'=>'required',
        //     'credit'=>'required'
        // ]);
        // return response()->json(['success'=>'Added new records.']);
    }
}
