<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use DataTables;

class ManageCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $existing = null;
        return view('admin.customers.index',compact('existing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:120|min:3',
            'company_name' => 'required|max:120|min:3',
            'email' => 'required|email',
            'credit_limit' => 'required|numeric',
            'contact_number' => 'required|regex:/[0-9]{11}/',
        ]);
        if($request->input('id'))
        {
            $id = $request->input('id');
            $existing = Customer::find($id);
            if($existing)
            {
                $existing->name = $request->input('name');
                $existing->company_name = $request->input('company_name');
                $existing->email = $request->input('email');
                $existing->contact_number = $request->input('contact_number');
                $existing->credit_limit = $request->input('credit_limit');
                $existing->save();
            }
            return redirect(route('customers.index'));
        }
        $this->validate($request,[
            'name' => 'required|max:120|min:3',
            'company_name' => 'required|max:120|min:3',
            'email' => 'required|email|unique:customers',
            'credit_limit' => 'required|numeric',
            'contact_number' => 'required|regex:/[0-9]{11}/',
        ]);
        $customer = new customer();
        $customer->name = $request->input('name');
        $customer->company_name = $request->input('company_name');
        $customer->email = $request->input('email');
        $customer->contact_number = $request->input('contact_number');
        $customer->credit_limit = $request->input('credit_limit');
        $customer->save();
        return redirect(route('customers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $existing = Customer::find($id);
        if($existing)
        {
            return view('admin.customers.index',compact('existing'));
        }
        return "Error";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function data()
    {
        $record = Customer::all();
        return DataTables::of($record)
            ->addColumn('action', function ($row){
                return '<a class="btn btn-primary btn-sm" href="'. route('customers.edit',['customer' => $row->id ]) . '">Edit<div class="ripple-container"></div></a>
                        <button onclick="return Confirm('.$row->id. ')" class="btn btn-danger btn-sm">Delete<div class="ripple-container"></div></button>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existing = Customer::find($id);
        if($existing)
        {
            $existing->delete();
            return response()->json(['data'=>true]);
        }
        else
        {
            return response()->json(['data'=>false]);
        }
    }
}
