<?php

namespace App\Http\Controllers\Admin;

use App\FinanceRecorder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Validator;
use Illuminate\Support\Facades\Auth;

class ManageFrController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $existing = null;
        $financeRecorders = FinanceRecorder::where('user_id','=',Auth::user()->id)->get(); 
        return view('admin.managefr.index',compact('existing','financeRecorders'));
    }

    public function data(){
        $user_id=Auth::user()->id;
       $record = FinanceRecorder::where('user_id',$user_id)->get();
        return DataTables::of($record)
            ->addColumn('action', function ($row){
                return '<a class="btn btn-primary btn-sm" href="'. route('managefr.edit',['fr' => $row->id ]) . '">Edit<div class="ripple-container"></div></a>
                        <button onclick="return Confirm('.$row->id. ')" class="btn btn-danger btn-sm">Delete<div class="ripple-container"></div></button>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|max:120|min:3',
            'description' => 'required|max:255',
        ]);
      
        if($validator->fails())
        {
            return redirect(route('managefr.index'))->withErrors($validator)->withInput();
        }

        if($request->input('id'))
        {
            $id = $request->input('id');
            $existing = FinanceRecorder::find($id);
            if($existing)
            {
                $existing->amount = $request->input('amount');
                $existing->description = $request->input('description');
                $existing->user_id = Auth::user()->id;//this->id;
                $existing->save();
            }
            return redirect(route('managefr.index'));
        }
        
            
            $existing = new FinanceRecorder();
            if($existing)
            {
                $existing->amount = $request->input('amount');
                $existing->description = $request->input('description');
                $existing->user_id = Auth::user()->id;//this->id;
                $existing->save();
            }
            return redirect(route('managefr.index'));
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FinanceRecorder  $financeRecorder
     * @return \Illuminate\Http\Response
     */
    public function show(FinanceRecorder $financeRecorder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FinanceRecorder  $financeRecorder
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $existing = FinanceRecorder::find($id);
        if($existing)
        {
            return view('admin.managefr.index',compact('existing'));
        }
        return "Error";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FinanceRecorder  $financeRecorder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinanceRecorder $financeRecorder)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FinanceRecorder  $financeRecorder
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existing = FinanceRecorder::find($id);

        if($existing)
        {
            $existing->delete();
            return response()->json(['data'=>true]);
        }
        else
        {
            return response()->json(['data'=>false]);
        }
    }
}
