<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\OpeningBalance;
use App\Account;
use DB;

class ManageOpeningBalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amount=null;
        $existing = null;
        $accounts_name = Account::leftJoin('opening_balances',function ($join){
            $join->on('accounts.id','=','opening_balances.account_id');
        })->select(['accounts.id','accounts.account_name','accounts.is_parent','opening_balances.account_id','opening_balances.debit','opening_balances.credit'])->get();
        return view('admin.openingbalance.index',compact('existing', 'accounts_name','amount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function get_data(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->submit=='debit')
        {
            $this->validate($request,[
                'account_id' => 'required',
                'amount' => 'required',
            ]);
            if($request->input('id'))
            {
                $id = $request->input('id');
                $existing = OpeningBalance::find($id);
                if($existing)
                {
                    $existing->account_id = $request->input('account_id');
                    $existing->debit = $request->input('amount');
                    $existing->credit = 0;
                    $existing->save();
                }
                return redirect(route('openingbalance.index'));
            }
            $openingbalance = new OpeningBalance();
            $openingbalance->account_id = $request->input('account_id');
            $openingbalance->debit = $request->input('amount');
            $openingbalance->credit = 0;
            $openingbalance->save();
            return redirect(route('openingbalance.index'));
        }
        $this->validate($request,[
            'account_id' => 'required',
            'amount' => 'required',
            ]);
            if($request->input('id'))
            {
                $id = $request->input('id');
                $existing = OpeningBalance::find($id);
                if($existing)
                {
                    $existing->account_id = $request->input('account_id');
                    $existing->debit = 0;
                    $existing->credit = $request->input('amount');
                    $existing->save();
                }
                return redirect(route('openingbalance.index'));
            }
        $openingbalance = new OpeningBalance();
        $openingbalance->account_id = $request->input('account_id');
        $openingbalance->debit = 0;
        $openingbalance->credit = $request->input('amount');;
        $openingbalance->save();
        return redirect(route('openingbalance.index'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $existing = OpeningBalance::find($id);
        if($existing)
        {
            return view('admin.openingbalance.index',compact('existing'));
        }
        return "Error";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $existing_row = OpeningBalance::find($id);
        $existing = $existing_row;
        if($existing)
        {
            $amount = 0;
            if($existing->debit == 0.00)
            {
                $amount = $existing->credit;
            }
            else
                {
                    $amount = $existing->debit;
                }
            $accounts_name = Account::leftJoin('opening_balances',function ($join){
                $join->on('accounts.id','=','opening_balances.account_id');
            })->select(['accounts.id','accounts.account_name','accounts.is_parent','opening_balances.account_id','opening_balances.debit','opening_balances.credit'])->get();
            return view('admin.openingbalance.index',compact('existing' , 'accounts_name','amount'));
        }
        return "Error";
    }
    public function data()
    {

        $record = OpeningBalance::all();
        //dd($records);
        return DataTables::of($record)
            ->addColumn('action', function ($row){
                return '<a class="btn btn-primary btn-sm" href="'. route('openingbalance.edit',['openingbalance' => $row->id ]) . '">Edit<div class="ripple-container"></div></a>
                        <button onclick="return Confirm('.$row->id. ')" class="btn btn-danger btn-sm">Delete<div class="ripple-container"></div></button>';
            })
            ->addColumn('account_name',function ($row){
                $acc = Account::find($row->account_id);
                if($acc)
                {
                    return $acc->account_name;
                }
            })
            ->rawColumns(['action','account_name'])
            ->make(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existing = OpeningBalance::find($id);
        if($existing)
        {
            $existing->delete();
            return response()->json(['data'=>true]);
        }
        else
        {
            return response()->json(['data'=>false]);
        }
    }
}
