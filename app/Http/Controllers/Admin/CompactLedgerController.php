<?php

namespace App\Http\Controllers\Admin;
//namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use App\Http\Controllers\Admin\Validator;
use App\Account;
use App\voucher_master;
use App\VoucherDetail;
use Validator;
use Session;
use Redirect;
use DB;
use DataTables;

class CompactLedgerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $existing = null;
        $user = auth()->user();
        $accountname = Account::select('id', 'account_name', 'user_id')
        ->where([
            ['user_id', $user->id],
        ['is_parent', '=', true],
        ['parent_id','=','0']
        ])->get();
        return view('admin.compactledgers.index', compact('accountname', 'existing'));


    }



    public function data(Request $request)
    {

        $this->startDate=$request->startDate;
        $this->endDate=$request->endDate;

                if($request->account==='0'&&empty($request->startDate)&&empty($request->endDate))
                {

                    $user = auth()->user();
                    //
                    $accounts = Account::select('id', 'account_name', 'user_id')
                    ->where([
                        ['user_id', $user->id],
                        ['is_parent', '=', true],
                        ['parent_id','!=','0']])->get();

                    return DataTables::of($accounts)
                    ->addColumn('debit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');


                        $vouchers = VoucherDetail::whereIn('account_id',$child_accounts)->get();                        $debit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $debit_sum += $value->debit;
                        }
                        return $debit_sum;
                    })
                    ->addColumn('credit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');

                        $vouchers = VoucherDetail::whereIn('account_id',$child_accounts)->get();
                        $credit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $credit_sum += $value->credit;
                        }
                        return $credit_sum;
                    })

                    ->rawColumns([ 'debit_sum', 'credit_sum'])
                    ->make(true);
                }
                elseif (empty($request->startDate)&&empty($request->endDate))
                {
                    $user = auth()->user();
                    //
                    $accounts = Account::select('id', 'account_name', 'user_id')
                    ->where([['user_id', $user->id],['is_parent', '=', true],['parent_id','=',$request->account]])->get();

                    return DataTables::of($accounts)
                    ->addColumn('debit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::whereIn('account_id',$child_accounts)->get();
                        $debit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $debit_sum += $value->debit;
                        }
                        return $debit_sum;
                    })
                    ->addColumn('credit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::whereIn('account_id',$child_accounts)->get();
                        $credit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $credit_sum += $value->credit;
                        }
                        return $credit_sum;
                    })

                    ->rawColumns([ 'debit_sum', 'credit_sum'])
                    ->make(true);
                }
                elseif (empty($request->account))
                {

                    $user = auth()->user();
                    //
                    $accounts = Account::select('id', 'account_name', 'user_id')
                    ->where([
                        ['user_id', $user->id],
                        ['is_parent', '=', true],
                        ['parent_id','!=','0']])->get();

                    return DataTables::of($accounts)
                    ->addColumn('debit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
                        ->join('voucher_masters', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
                        ->whereBetween('voucher_masters.date', [
                            date("Y-m-d", strtotime($this->startDate)),
                            date("Y-m-d", strtotime($this->endDate))])
                        ->whereIn('account_id',$child_accounts)->get();                        $debit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $debit_sum += $value->debit;
                        }
                        return $debit_sum;
                    })
                    ->addColumn('credit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
                        ->join('voucher_masters', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
                        ->whereBetween('voucher_masters.date', [
                            date("Y-m-d", strtotime($this->startDate)),
                            date("Y-m-d", strtotime($this->endDate))])
                        ->whereIn('account_id',$child_accounts)->get();
                        $credit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $credit_sum += $value->credit;
                        }
                        return $credit_sum;
                    })

                    ->rawColumns([ 'debit_sum', 'credit_sum'])
                    ->make(true);
                }
                else{
                    $user = auth()->user();
                    //
                    $accounts = Account::select('id', 'account_name', 'user_id')
                    ->where([['user_id', $user->id],['is_parent', '=', true],['parent_id','=',$request->account]])->get();

                    return DataTables::of($accounts)
                    ->addColumn('debit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
                        ->join('voucher_masters', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
                        ->whereBetween('voucher_masters.date', [
                            date("Y-m-d", strtotime($this->startDate)),
                            date("Y-m-d", strtotime($this->endDate))])
                        ->whereIn('account_id',$child_accounts)->get();
                        $debit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $debit_sum += $value->debit;
                        }
                        return $debit_sum;
                    })
                    ->addColumn('credit_sum', function ($row) {
                        $child_accounts = Account::where('parent_id','=',$row->id)
                        ->where('is_parent','=',false)->select('id')->get()->pluck('id');
                        $vouchers = VoucherDetail::join('accounts', 'accounts.id', '=', 'voucher_details.account_id')
                        ->join('voucher_masters', 'voucher_details.voucher_master_id', '=', 'voucher_masters.id')
                        ->whereBetween('voucher_masters.date', [
                            date("Y-m-d", strtotime($this->startDate)),
                            date("Y-m-d", strtotime($this->endDate))])
                        ->whereIn('account_id',$child_accounts)->get();                        $credit_sum = 0;
                        foreach ($vouchers as $key => $value) {
                            $credit_sum += $value->credit;
                        }
                        return $credit_sum;
                    })

                    ->rawColumns([ 'debit_sum', 'credit_sum'])
                    ->make(true);
                }


    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
