<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
    public function __construct()
    {
      $this->middleware('auth');
    }
    */
    public function __construct()
    {

        $this->middleware('guest')->except('logout');
    }


    //pam-redirect to custom login when type /login
    public function showLoginForm()
    {
        if (Auth::check()){
            return redirect('/admin/dashboard');
        }
        else
            return view('custom-auth.login');
    }
}
