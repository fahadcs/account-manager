<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinanceRecorder extends Model
{
    protected $fillable = [
        'user_id',
        'amount',
        'description',
    ];
}
