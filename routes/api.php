<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\LoginController@login')->name('apiLogin');

Route::group(['middleware' => 'verifytoken'], function() {
    Route::post('test', 'Api\TestController@test');
    Route::post('manage-fr', 'API\ManageFrController@index');
    Route::post('manage-fr/create', 'API\ManageFrController@store');
    Route::put('manage-fr/update/{id}', 'API\ManageFrController@show');
    Route::delete('manage-fr/delete/{id}', 'API\ManageFrController@destroy');
   });
