<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);
Route::get('/', function () {
    return view('landingpage.index');
});
/*
Route::get('/', function () {
    return view('custom-auth.login');
});

Route::get('/vouchers',function () {
    return view('custom-views.voucher_master');
});*/
//Auth::routes();
Route::post('/','ContactUsController@index')->name('contact_us');

Route::group(['namespace'=>'Admin','prefix'=>'admin','as','admin.', 'middleware' => ['admin','auth']],function (){
    Route::get('/dashboard','AdminDashboardController@index')->name('dashboard');
    Route::get('/vendor-data','ManageVendorController@data')->name('vendor.data');
    Route::get('/customer-data','ManageCustomerController@data')->name('customer.data');
    Route::get('/account-data','ManageAccountController@data')->name('account.data');
    Route::get('/openingbalance-data','ManageOpeningBalanceController@data')->name('openingbalance.data');
    Route::get('/voucher-data','ManageVoucherController@data')->name('voucher.data');
    Route::post('/ledger-data','LedgerController@data')->name('ledger.data');
    Route::post('/compact-ledger-data','CompactLedgerController@data')->name('compactledger.data');



    //Route::get('/vouchers-data','ManageVoucherController@data')->name('vouchers.data');

    Route::resource('vendors','ManageVendorController');
    Route::resource('customers','ManageCustomerController');
    Route::resource('accounts','ManageAccountController');
    Route::resource('openingbalance','ManageOpeningBalanceController');
    Route::resource('vouchers','ManageVoucherController');
    Route::resource('ledgers','LedgerController');
    Route::resource('compact-ledger','CompactLedgerController');


    Route::post('saveform','ManageVoucherController@saveform')->name('save-form');
    Route::post('delete','ManageVoucherController@delete')->name('delete-data');
    Route::post('showdetail','ManageVoucherController@showdetail')->name('show-detail');

    // Finance Recorders
    Route::resource('managefr', 'ManageFrController');
    Route::get('/fr-data','ManageFrController@data')->name('managefr.data');

});

Route::get('/home', 'HomeController@index')->name('home');
