<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'company_name' => $faker->unique()->name,
        'contact_number' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'credit_limit' => 1000,
    ];
});
