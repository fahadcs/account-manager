<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Vendor;
use Faker\Generator as Faker;

$factory->define(Vendor::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'company_name' => $faker->name,
        'contact_number' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'debit_limit' => 1000,
    ];
});
