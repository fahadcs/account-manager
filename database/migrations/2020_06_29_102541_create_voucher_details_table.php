<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('voucher_master_id')->unsigned();
            $table->index('voucher_master_id');
            $table->foreign('voucher_master_id')->references('id')->on('voucher_masters')->onDelete('cascade');
            $table->string('narration');
            $table->float('debit');
            $table->float('credit');
            $table->bigInteger('account_id')->unsigned();
            $table->index('account_id');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('voucher_details');
    }
}
