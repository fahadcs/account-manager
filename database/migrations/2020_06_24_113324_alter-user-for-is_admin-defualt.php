<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserForIsAdminDefualt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*///////////
        if (Schema::hasColumn('users', 'is_admin')){

            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('is_admin');
            });
        }
        ////////////*/
        if (Schema::hasTable('users')) {
            Schema::table('users', function (Blueprint $table) {
                if (Schema::hasColumn('users', 'is_admin')) {
                    $table->boolean('is_admin')->default(1)->change();
                }
            });
        }

        //Schema::table('users', function (Blueprint $table) {
        //    $table->boolean('is_admin')->defualt(1)->change();
        //});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
